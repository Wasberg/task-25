const { Sequelize, QueryTypes } = require("sequelize");

require('dotenv').config();

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect() {

    try {

        await sequelize.authenticate();
        const countries = await sequelize.query('SELECT name FROM country',{ type: QueryTypes.SELECT});
        const cities = await sequelize.query('SELECT name FROM city',{ type: QueryTypes.SELECT});
        const languages =  await sequelize.query('SELECT DISTINCT language FROM countrylanguage',{ type: QueryTypes.SELECT});
        console.log(countries);
        console.log(cities);
        console.log(languages);
        await sequelize.close();
    }
    catch (e) {
        console.error(e);
    }

}

connect();